import * as React from 'react';
import {TextInput,StyleSheet,Text ,Button, View,ImageBackground,ScrollView} from 'react-native';
import Container from './src/Component/Container'
import {CustomButton} from './src/Component/CustomButton'
import {PrimaryTheme} from './src/Style/Themes'
import { Formik } from 'formik';
import {Vadidators} from './src/utills/Vadidators'
import If from './src/Component/If'
import {widthPercentageToDP as wp ,listenOrientationChange,removeOrientationListener} from 'react-native-responsive-screen' 
import {Responsive} from './src/utills/Responsive'
import {pComponentStyleS,lComponentStyleS, Typography} from './src/Style/Golble'
import CustomText from './src/Component/CustomText'

interface State{
   from:{
    FristNameInuputValue:string;
    LastNameInuputValue:string;
    emailInuputValue:string;
    PasswordInuputValue:string;
   }
   orientation :string
}
interface Props{
}


export class App extends React.Component<Props,State>{
  
  private passwordinputRef;
    private LastNameputRef;
      private emailIputRef;
    constructor(props) {
      super(props);
      this.state = {
      from:{
        FristNameInuputValue:'',
        LastNameInuputValue:'',
        emailInuputValue: '',
        PasswordInuputValue:'',
      },
      orientation:'portrait'
      }
    }
    componentDidMount(){
      listenOrientationChange(this)
    }
    componentWillUnmount(){
      removeOrientationListener();
    }
  
  render(){
    // const pStyle = portraitStyleS();
    // const lStyle = landSacpeStyle();

    return( 
      <Container containerStyle={{alignItems:"center"}}>
        <CustomText 
          style={[Typography.title,]}>Registration Form
        </CustomText>
          <Formik 
            validationSchema={Vadidators.logoinVadidators}
            initialValues={this.state.from}
            validateOnMount={true}
            validateOnChange={true}
            onSubmit={()=>{console.log('sumbit')}}
          >
          {(props)=>{
            return(
                <View style={{alignItems:"center"}}>
                  <TextInput 
                    onSubmitEditing={()=>this.passwordinputRef.focus()}
                    returnKeyType={'next'}
                    onChangeText={props.handleChange('FristNameInuputValue')}
                    style={Responsive.dymamicesponsive
                      (pComponentStyleS.inputstyle,lComponentStyleS.inputstyle,
                        this.state.orientation)}
                    placeholder={'FristName'}
                    value={props.values.FristNameInuputValue}
                    onBlur={()=>props.setFieldTouched('FristNameInuputValue')}
                  />
                  <If show={props.dirty && props.touched.FristNameInuputValue}>
                    <CustomText style={[Typography.errorText,]}>
                      {props.errors.FristNameInuputValue}
                    </CustomText> 
                  </If>
                
                  <TextInput
                    ref={ref => this.passwordinputRef = ref}
                    onSubmitEditing={()=>this.LastNameputRef.focus()}
                    returnKeyType={'next'}
                    onChangeText={props.handleChange('LastNameInuputValue')}
                    style={Responsive.dymamicesponsive
                      (pComponentStyleS.inputstyle,lComponentStyleS.inputstyle,
                        this.state.orientation)}
                    placeholder={'LastName'}
                    value={props.values.LastNameInuputValue}
                    onBlur={()=>props.setFieldTouched('LastNameInuputValue')}
                  />
                  <If show={props.dirty && props.touched.LastNameInuputValue}>
                  <CustomText style={[Typography.errorText,]}>
                      {props.errors.LastNameInuputValue}
                    </CustomText>
                  </If>
                  
                  <TextInput 
                    onSubmitEditing={()=>this.emailIputRef.focus()}
                    ref={ref => this.LastNameputRef = ref}   
                    returnKeyType={'next'}
                    onChangeText={props.handleChange('emailInuputValue')}
                    style={Responsive.dymamicesponsive
                      (pComponentStyleS.inputstyle,lComponentStyleS.inputstyle,
                        this.state.orientation)}
                    placeholder={'email'}
                    value={props.values.emailInuputValue}
                    onBlur={()=>props.setFieldTouched('emailInuputValue')}
                  />
                  <If show={props.dirty && props.touched.emailInuputValue}>
                  <CustomText style={[Typography.errorText,]}>
                    {props.errors.emailInuputValue}
                  </CustomText>
                  </If>
                  
                  <TextInput 
                    onSubmitEditing={()=>{
                      if(props.isValid){
                        console.log("Is Valid")
                          }else{
                            console.log("from is not Valid")
                       }
                     }}
                    returnKeyType={'done'}
                    ref={ref => this.emailIputRef = ref}
                    onChangeText={props.handleChange('PasswordInuputValue')}
                    style={Responsive.dymamicesponsive
                      (pComponentStyleS.inputstyle,lComponentStyleS.inputstyle,
                        this.state.orientation)}
                    placeholder={'Password'}
                    value={props.values.PasswordInuputValue}
                    onBlur={()=>props.setFieldTouched('PasswordInuputValue')}
                  />
                  <If show={props.dirty && props.touched.PasswordInuputValue}>
                     
                  <CustomText style={[Typography.errorText,]}>
                      {props.errors.PasswordInuputValue}
                    </CustomText>
                  </If>
                   
                  <CustomButton  disabled={!props.isValid}
                  onPress={()=>{
                    if(props.isValid){
                      return  props.handleSubmit();
                    }else{
                      console.log("from is not Valid")
                    }}}
                    title="submit"  
                  />
                   
              </View>
            )
          }}
         </Formik>
      </Container>
    )
  }
}


const portraitStyleS = () =>{
 return StyleSheet.create(({
    // inputstyle:{
    //     borderRadius: 10,
    //     marginBottom: 20,
    //     borderColor:PrimaryTheme.$BLACK_COLOR,
    //     width:wp('80%'),
    //     borderBottomColor: PrimaryTheme.$BLACK_COLOR,
    //     borderBottomWidth: 2,
    // }
  }))
}
const landSacpeStyle  = ()=>{
  return  StyleSheet.create(({
    // inputstyle:{
    //   ...portraitStyleS().inputstyle,
    //   width:wp('80%'),
    // }
  }))
}