import * as React from 'react';
import { SafeAreaView,StyleSheet, ViewStyle, } from 'react-native';

interface Props{
    children: any;
    containerStyle?: ViewStyle | ViewStyle[]
}
interface State{
    
}
const Container = (props:Props)=>{
return(
    <SafeAreaView style={[style.container, props.containerStyle]}>
        {props.children}
    </SafeAreaView>
)
}


const style = StyleSheet.create(({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
    }
}))

export default Container;