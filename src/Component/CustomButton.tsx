import React from 'react';
import { TouchableOpacity,Text,StyleSheet, TextStyle } from 'react-native';
import {PrimaryTheme} from '../Style/Themes'
import {widthPercentageToDP as wp,
     heightPercentageToDP as hp,
    listenOrientationChange,removeOrientationListener
    } from 'react-native-responsive-screen';
import { Responsive } from '../utills/Responsive';

export interface Props{
    onPress?: any;
    title:string;
    disabled:any;
    buttonstyle?: TextStyle | TextStyle[] ;
}
export interface State{
    orientation :string
}

export class CustomButton extends React.Component<Props,State>{
    static defaultProps: { disabled: boolean; };
    constructor(props) {
        super(props);
        this.state = {
        orientation:'portrait'
        }
      }
      componentDidMount(){
        listenOrientationChange(this)
      }
      componentWillUnmount(){
        removeOrientationListener();
      }
    render() {
         const pbuttonStyle = buttonportraitStyleS();
         const lbuttonStyle = buttonplandSacpeStyle();
      return (
        <TouchableOpacity 
            disabled={this.props.disabled} 
               style={[Responsive.dymamicesponsivebutton
                (this.props.disabled ? {...pbuttonStyle.buttonstyle,
                     backgroundColor:"red"
                 }  :
                 pbuttonStyle.buttonstyle,lbuttonStyle.buttonstyle,
                    this.state.orientation)]} onPress={this.props.onPress}>
                <Text  style={[style.Textbuttonstyle]}>{this.props.title}</Text>
        </TouchableOpacity> 
      );
    }
  }
const buttonportraitStyleS = () =>{
    return StyleSheet.create(({
        buttonstyle:{
            height: hp('6%'),
            width: wp('36%'),
            alignItems:"center",
            borderRadius:5,
            backgroundColor:PrimaryTheme.$BUTTON_COLOR
        },
    }))
}
const buttonplandSacpeStyle = () =>{
    return StyleSheet.create(({
        buttonstyle:{
           ...buttonportraitStyleS().buttonstyle,
           height: hp('6%'),
           width: wp('45%'),
        },
    }))
}
const style = StyleSheet.create(({
    Textbuttonstyle:{
    alignItems:"center",
    color:"#fff",
    paddingTop: 12,
    fontSize:17,
}
}))
 CustomButton.defaultProps = {
    disabled:false
}

