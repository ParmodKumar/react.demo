import * as React from 'react';
import { PrimaryTheme } from './Themes';
import {widthPercentageToDP as wp ,
heightPercentageToDP as hp
} from 'react-native-responsive-screen' 

export const pComponentStyleS = {
    inputstyle:{
        borderRadius: 10,
        marginBottom: 20,
        borderColor:PrimaryTheme.$BLACK_COLOR,
        width:wp('80%'),
        borderBottomColor: PrimaryTheme.$BLACK_COLOR,
        borderBottomWidth: 2,
    }
}

export const lComponentStyleS = {
    inputstyle:{
        ...pComponentStyleS.inputstyle,
        width:wp('140%'),
}
}

export const Typography : any = {
    title:{
    fontSize:hp('3%'),
    color:PrimaryTheme.$TEXT_COLOR_900,
    fontWeight:'bold',
    fontFamily:'Muli',
    marginBottom: wp("2%"),
    letterSpacing:1,
    }, 
    heading:{
        fontSize:hp('8%'),
        color:PrimaryTheme.$TEXT_COLOR_900,
        fontWeight:"bold"
    },
    subheading:{
        fontSize:hp('5%'),
        color:PrimaryTheme.$TEXT_COLOR_700,
        fontFamily:'Muli',
        fontWeight:"500"
    },
    paragraph:{
        fontSize:hp('4%'),
        color:PrimaryTheme.$TEXT_COLOR_900,
        fontFamily:'Muli',
    },
    lighText:{
        fontSize:hp('3.2%'),
        color:PrimaryTheme.$TEXT_COLOR_500,
        fontFamily:'Muli',
    },
    errorText:{
        fontSize:hp('1.7%'),
        color:PrimaryTheme.$Red_COLOR,
        fontFamily:'Muli',
        fontWeight:"500"

    }
}

