import * as React from 'react';
import { PrimaryTheme } from './Themes';
import {widthPercentageToDP as wp ,
heightPercentageToDP as hp
} from 'react-native-responsive-screen' 



 const pbutton = {
    buttonstyle:{
        height: hp('6%'),
        width: wp('36%'),
        alignItems:"center",
        borderRadius:5,
        backgroundColor:PrimaryTheme.$BUTTON_COLOR
    },
}
export const lbutton = {
    buttonstyle:{
       ...pbutton.buttonstyle,
       height: hp('6%'),
       width: wp('45%'),
    },
}
