export enum PrimaryTheme {
    $BLACK_COLOR = "#0e0d0d",
    $BUTTON_COLOR = "#4272d7",
    $TEXT_COLOR_900 = "#0e0d0d",
    $TEXT_COLOR_700 = "#0e0d0d",
    $TEXT_COLOR_500 = "#0e0d0d",
    $Red_COLOR = "red"
}